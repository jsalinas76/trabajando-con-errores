import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler, Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GestorErroresGlobalService implements ErrorHandler{

  constructor(private router: Router) { }

  handleError(error: any): void {

    if(error instanceof HttpErrorResponse){
      // Errores relacionados con el backend


      // ...
      console.error(error.message);
    }else{
      // Errores en el cliente

      this.router.navigateByUrl('/error');
      console.error(error.message);

    }


  }
}
