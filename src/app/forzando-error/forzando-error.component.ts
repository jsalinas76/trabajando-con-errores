import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forzando-error',
  templateUrl: './forzando-error.component.html',
  styleUrls: ['./forzando-error.component.css']
})
export class ForzandoErrorComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  enviarError(): void{
    throw new Error("Se está produciendo una excepción ...");
  }

}
