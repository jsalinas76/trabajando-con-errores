import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForzandoErrorComponent } from './forzando-error.component';

describe('ForzandoErrorComponent', () => {
  let component: ForzandoErrorComponent;
  let fixture: ComponentFixture<ForzandoErrorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForzandoErrorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForzandoErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
