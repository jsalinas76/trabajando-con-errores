import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorPanelComponent } from './error-panel/error-panel.component';
import { ForzandoErrorComponent } from './forzando-error/forzando-error.component';

const routes: Routes = [
  {path: 'error', component: ErrorPanelComponent},
  {path: 'inicio', component: ForzandoErrorComponent},
  {path: '', redirectTo: 'inicio', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
