import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ForzandoErrorComponent } from './forzando-error/forzando-error.component';
import { GestorErroresGlobalService } from './gestor-errores-global.service';
import { ErrorPanelComponent } from './error-panel/error-panel.component';

@NgModule({
  declarations: [
    AppComponent,
    ForzandoErrorComponent,
    ErrorPanelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [{provide: ErrorHandler, useClass: GestorErroresGlobalService}],
  bootstrap: [AppComponent]
})
export class AppModule { }
