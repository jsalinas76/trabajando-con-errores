import { TestBed } from '@angular/core/testing';

import { GestorErroresGlobalService } from './gestor-errores-global.service';

describe('GestorErroresGlobalService', () => {
  let service: GestorErroresGlobalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestorErroresGlobalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
